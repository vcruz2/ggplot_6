Silver Woods
================

## Aplicando Gggplot, a Silverwoods- Librerias

## Datos

``` r
clima <- read.table("https://drvcruz.s3.us-east-2.amazonaws.com/SilwoodWeather.txt",header = T)
```

## Configuracion de DB

``` r
attach(clima)
head(clima)
```

      upper lower rain month   yr
    1  10.8   6.5 12.2     1 1987
    2  10.5   4.5  1.3     1 1987
    3   7.5  -1.0  0.1     1 1987
    4   6.5  -3.3  1.1     1 1987
    5  10.0   5.0  3.5     1 1987
    6   8.0   3.0  0.1     1 1987

``` r
str(clima)
```

    'data.frame':   6940 obs. of  5 variables:
     $ upper: num  10.8 10.5 7.5 6.5 10 8 5.8 2.8 -0.8 1.5 ...
     $ lower: num  6.5 4.5 -1 -3.3 5 3 -3.3 -5.5 -4.8 -1 ...
     $ rain : num  12.2 1.3 0.1 1.1 3.5 0.1 0 0 0 0 ...
     $ month: int  1 1 1 1 1 1 1 1 1 1 ...
     $ yr   : int  1987 1987 1987 1987 1987 1987 1987 1987 1987 1987 ...

``` r
month <- factor(month)
is.factor(month)
```

    [1] TRUE

## Gráfica Completa de cada mes vs upper limit

``` r
(p <- ggplot(clima, aes(x = factor(month), y = upper)) +
      geom_boxplot())
```

![](Silverwood_files/figure-gfm/unnamed-chunk-4-1.png)<!-- -->

``` r
(p1 <- ggplot(clima, aes(x = factor(yr), y = lower)) +
      geom_boxplot())
```

![](Silverwood_files/figure-gfm/unnamed-chunk-4-2.png)<!-- -->

``` r
(p <- ggplot(clima, aes(x = factor(month), y = upper)) + 
      geom_boxplot() +
      labs(title = "Grafica de Temp. vs Limites Altos de Temp ",
           subtitle = "Bosque : Silverwood",
            x = "MES del Año", y = "Temp.Altas"))
```

![](Silverwood_files/figure-gfm/unnamed-chunk-5-1.png)<!-- -->

## Usando Plottly

``` r
# ggplotly(p)
```

## Analisis para todos los años del mes 3 en En rango Bajo

``` r
subClima <- subset(clima, month =="3")
head(subClima)
```

       upper lower rain month   yr
    60  14.5   7.5  5.6     3 1987
    61  11.3   6.8  1.4     3 1987
    62   9.5   0.3  0.0     3 1987
    63   2.8  -3.3  0.9     3 1987
    64   3.5  -0.8  0.2     3 1987
    65   8.3   1.8  1.0     3 1987

## AHORA EL FACTOR ES AÑO

``` r
(mes3 <- ggplot(subClima, aes(x = factor(yr), y = lower)) + 
        geom_boxplot() +
        labs(title= "Grafica de Temp. vs Limite Alto ",
             subtitle = "Silverwood",
             x= "MES") )
```

![](Silverwood_files/figure-gfm/unnamed-chunk-8-1.png)<!-- -->

``` r
# ggplotly(mes3)
```

## Analisis de la Media mes 3

``` r
(m <- median(subClima$lower))
```

    [1] 3.3

``` r
(mes3 <- ggplot(subClima, aes(x = factor(yr), y = lower)) + 
        geom_boxplot() +
        labs(title= "Grafica de Temp. vs Limite Alto ",
             subtitle = "Silverwood",
             x= "MES") +
        geom_hline(yintercept = m))
```

![](Silverwood_files/figure-gfm/unnamed-chunk-9-1.png)<!-- -->

``` r
# ggplotly(mes3)
```

## Analisis del Boxplot Mes 3

``` r
 # Cuantiles del mes 3 para todos los años 1987-2005

(p <- ggplot(subClima, aes( y = lower)) + 
      geom_boxplot() )
```

![](Silverwood_files/figure-gfm/unnamed-chunk-10-1.png)<!-- -->

``` r
# ggplotly(p)
```

``` r
quantile(subClima$lower)#SOLO PARA EL MES 3
```

      0%  25%  50%  75% 100% 
    -7.3  0.5  3.3  6.0 12.0 

``` r
sd(subClima$lower)
```

    [1] 3.733525

``` r
subClima2 <- subset(clima, yr =="1996")
head(subClima2,10)
```

         upper lower rain month   yr
    3289   3.5  -0.5  0.3     1 1996
    3290   6.0   1.0  0.3     1 1996
    3291   6.3   2.5  0.0     1 1996
    3292   6.0   2.0  0.0     1 1996
    3293   7.5   3.5  3.6     1 1996
    3294   8.0   4.5  3.1     1 1996
    3295  10.0   4.5  6.8     1 1996
    3296  11.0   5.8  1.8     1 1996
    3297  12.0   6.5 22.0     1 1996
    3298  11.0   5.0  0.0     1 1996

## AHORA EL FACTOR ES AÑO

``` r
(mes3 <- ggplot(subClima2, aes(x = factor(month), y = lower)) + 
        geom_boxplot() +
        labs(title= "Grafica de Temp. vs Limite Alto ",
             subtitle = "Silverwood",
             x= "MES") )
```

![](Silverwood_files/figure-gfm/unnamed-chunk-13-1.png)<!-- -->

``` r
# ggplotly(mes3)
```
