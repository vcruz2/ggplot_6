lluvia
================

``` r
#install.packages("esquisse")
```

``` r
(p <- ggplot(clima) +
 aes(x = factor(yr), y = rain) +
 geom_boxplot(shape = "circle", fill = "#FF69B4") +
 labs(x = "año", 
 y = "mm lluvia", 
 title = "Año vs mm Rain",
 subtitle = "silverwood") +
 theme_dark())
```

![](lluvia_files/figure-gfm/lluvia-1.png)<!-- -->

``` r
# ggplotly(p)
```
